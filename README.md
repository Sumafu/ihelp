# iHelp #

You are tired of the default /help command in Bukkit? iHelp is the solution! iHelp integrates a better /help command to your Minecraft server, based on inventories.

If want to have also the default /help command, you can use `/?` or `/bukkit:help` instead.

## Setup ##
Download the latest version of the plugin at the left side under Downloads. Unpack the Zip-file, copy the jar-File into the plugins folder and restart the server. That was it.
Now you can use the better /help command. At time it includes a default menu, see more under "Configuration"

## Configuration ##
In the plugins folder you find a folder named iHelp, in this folder is a file with name 'iHelpMenu.yml', this file includes the hole data for the menu. If you changed something in the configuration, you must reload the server at least with `/reload`. 

In every title you can use formatting codes with the § sign, like §4 or §f or §l or something else.

### Views ###
The node `help.views` includes all inventory views of the plugin. The structure of this node is like the following:

    help:
      views:
        - name: <name of view>
          title: <title of view, displayed ingame>
          rows: <number of rows, 1 to 6>
          shortcut: <optional, name of a shortcut: /help shortcut>
          items:
            <items definition>
        - <another view>
        - <another view>

The items are implements like the following:

    help:
      views:
        - <view>
          items:
            - position: <position of item, beginns with 0>
              itemId: <minecraft id of block/item, like minecraft:stone>
              title: <title of the item>
              target: <target>
              lore:
                - Lore is optional
                - A text under the title of the item
            - <another item>
            - <another item>

The target node show, which view or text has to be shown or which command should be executet if a player clicks on the item.
If the target should be a view, it looks like this: `view:<name>`, if the target is a text, this: `text:<name>`, if the target is a command,
this: `command:<name>`

#### Main view ####
The main view is defined in this node:

    help:
     name_of_start_view: <name of first view>

This view will be shown with the `/help` command.

### Texts ###
The node `help.texts` inlcudes all texts, that can be send to the player. Here is the structure of a text node:

    help:
      texts:
        - name: <name of text>
          shortcut: <optional, name of a shortcut: /help shortcut>
          text:
            - Here you define the text that should be displayed.
            - Theoretically you can use a unlimitted number of lines
            - You can also use formatting code like §f for white or §l for Bold
        - <another text>
        - <another text>

### Commands ###
You can also define commands as target of an item, here is the implementation:

    help:
      commads:
        - name: <name of command>
          command: <command itself, like 'time set day'>
          executeAsConsole: <true if executed with all permissions or false if executed with players permissions>
        - <another command>
        - <another command>

You cannot define shortcuts for commands.

## Shortcuts ##
In the configuration section you read soemthing about shortcuts. Shortcuts are an easy way to got directly to an view or text without clicking through the menu. You can define shortcuts for views and texts, not for commands, because this would be stupid.

If you have a created a shortcut, you can use this like `/help <shortcut>`. An example: You created a shotcut with name "cheese" for a view, than you can type `/help cheese` and the defined view will be opend. You can also define multiple words as shortcut. An example: "cheese cake", than you type `/help cheese cake`.
