package de.sumafu.iHelp.main;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.server.v1_8_R3.Item;
import net.minecraft.server.v1_8_R3.MinecraftKey;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ViewController {
	public ViewController(iHelp plugin){
		this.plugin = plugin;
	}
	
	private iHelp plugin = null;
	
	private FileConfiguration invFileMenu = null;
	private File invFile = null;
	
	private String mainView = "";
	private Map<String, Inventory> views = new HashMap<String, Inventory>();
	private Map<String, String[]> targets = new HashMap<String, String[]>();
	private Map<String, String[]> texts = new HashMap<String, String[]>();
	private Map<String, Map<String, Object>> commands = new HashMap<String, Map<String, Object>>();
	private Map<String, String> shortcuts = new HashMap<String, String>();
	
	Inventory getMainView(){
		return this.views.get(this.mainView);
	}
	
	Inventory getViewWithName(String name){
		return this.views.get(name);
	}
	
	String getTarget(String invName, int itemPosition){
		String[] view = this.targets.get(invName);
		
		String target = view[itemPosition];
		
		return target;
	}
	
	String[] getText(String textName){
		return this.texts.get(textName);
	}
	
	Map<String, Object> getCommand(String commandName){
		return this.commands.get(commandName);
	}
	
	String getShortcut(String shortcut){
		return this.shortcuts.get(shortcut);
	}
	
	void loadData(){
		if(this.invFileMenu==null)return;
		
		this.mainView = this.invFileMenu.getString("help.name_of_start_view");
		
		
		// Load views
		@SuppressWarnings("unchecked")
		List<LinkedHashMap<String, Object>> viewList = (List<LinkedHashMap<String, Object>>) this.invFileMenu.getList("help.views");
		
		this.views.clear();
		for(LinkedHashMap<String, Object> view : viewList){
			String viewName = (String) view.get("name");
			String viewTitle = (String)view.get("title");
			int viewSize = ((int)view.get("rows"))*9;
			iHelpInventory viewHolder = new iHelpInventory(viewName);
			
			Inventory myInv = Bukkit.createInventory(viewHolder, viewSize, viewTitle);
			
			String[] myTargets = new String[viewSize];
			
			@SuppressWarnings("unchecked")
			ArrayList<LinkedHashMap<String, Object>> itemList = (ArrayList<LinkedHashMap<String, Object>>) view.get("items");
			
			for(LinkedHashMap<String, Object> item : itemList){
				
				int itemPosition = (int)item.get("position");
				String itemId = (String)item.get("itemId");
				String itemTitle = (String)item.get("title");
				String target = (String)item.get("target");
				
				Material myMaterial = null;
				MinecraftKey key = new MinecraftKey(itemId);
				Item item1 = Item.REGISTRY.get(key);
				if(item1!=null){
					myMaterial =  CraftItemStack.asNewCraftStack(item1).getType();
				}else{
					myMaterial = Material.STONE;
				}
				
				@SuppressWarnings("unchecked")
				ArrayList<String> loreList = (ArrayList<String>)item.get("lore");
				if(loreList!=null)
				for(int i=0;i<loreList.size();i++){
					loreList.set(i, "§5"+loreList.get(i));
				}
				
				ItemStack myItem = new ItemStack(myMaterial);
				
				ItemMeta myItemMeta = myItem.getItemMeta();
				myItemMeta.setDisplayName("§f"+itemTitle);
				myItemMeta.setLore(loreList);
				
				myItem.setItemMeta(myItemMeta);
				myInv.setItem(itemPosition, myItem);
				
				myTargets[itemPosition] = target;
			}
			String shortcut = (String)view.get("shortcut");
			if(shortcut!=null){
				this.shortcuts.put(shortcut, "view:"+viewName);
			}
			
			
			
			this.targets.put(viewName, myTargets);
			this.views.put(viewName, myInv);
		}
		
		// Load texts
		@SuppressWarnings("unchecked")
		List<LinkedHashMap<String, Object>> textList = (List<LinkedHashMap<String, Object>>) this.invFileMenu.getList("help.texts");
		for(LinkedHashMap<String, Object> text : textList){
			String name = (String)text.get("name");
			@SuppressWarnings("unchecked")
			ArrayList<String> texte = (ArrayList<String>) text.get("text");
			String[] myTexte = texte.toArray(new String[texte.size()]);
			this.texts.put(name, myTexte);
			
			String shortcut = (String)text.get("shortcut");
			if(shortcut!=null){
				this.shortcuts.put(shortcut, "text:"+name);
			}
		}
		
		// Load commands
		@SuppressWarnings("unchecked")
		List<LinkedHashMap<String, Object>> commandList = (List<LinkedHashMap<String, Object>>) this.invFileMenu.getList("help.commands");
		if(commandList!=null)
		for(LinkedHashMap<String, Object> command : commandList){
			String name = (String)command.get("name");
			String theCommand = (String)command.get("command");
			boolean console = (boolean)command.get("executeAsConsole");
			
			Map<String, Object> myCmd = new HashMap<String, Object>();
			myCmd.put("command", theCommand);
			myCmd.put("console", console);
			
			this.commands.put(name, myCmd);
		}
	}
	
	void loadMenuFile(){
		if(!this.plugin.getDataFolder().exists())this.plugin.getDataFolder().mkdir();
		
		this.invFile = new File(this.plugin.getDataFolder(), "iHelpMenu.yml");
		
		if(!this.invFile.exists()){
			this.invFileMenu = YamlConfiguration.loadConfiguration(this.invFile);
			
			InputStream defConfigStream = this.plugin.getResource("iHelpMenu.yml");
			if(defConfigStream!=null){
				YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream));
		        this.invFileMenu.setDefaults(defConfig);
		        try {
		        	this.invFileMenu.options().copyDefaults(true);
					this.invFileMenu.save(invFile);
					this.invFileMenu.options().copyDefaults(false);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}else{
			this.invFileMenu = YamlConfiguration.loadConfiguration(this.invFile);
		}
	}
}
