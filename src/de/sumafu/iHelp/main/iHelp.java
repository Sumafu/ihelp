package de.sumafu.iHelp.main;

import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

public class iHelp extends JavaPlugin implements Listener {
	
	ViewController vc = null;
	
	public void onEnable(){ 
		 this.getServer().getPluginManager().registerEvents(this, this);
		 
		 vc = new ViewController(this);
		 vc.loadMenuFile();
		 vc.loadData();
		 
		 
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if(cmd.getName().equalsIgnoreCase("help")){
			Player player = null;
			if(sender instanceof Player){
				player = (Player) sender;
			}else{
				sender.sendMessage(ChatColor.RED+"You cannot access this command with console!");
				return true;
			}
			
			if(args.length==0){
				Inventory mainView = this.vc.getMainView();
				player.openInventory(mainView);
			}else{
				StringBuilder sb = new StringBuilder();
				for(int i=0;i<args.length;i++){
					sb.append(args[i]);
					if(i!=args.length-1)sb.append(" ");
				}
				String action = this.vc.getShortcut(sb.toString());
				if(action==null)sender.sendMessage(ChatColor.RED+"Cannot find this command!");
				else{
					switch(action.split(":")[0]){
					case "view":
						Inventory nextView = this.vc.getViewWithName(action.split(":")[1]);
						player.openInventory(nextView);
						break;
					case "text":
						player.closeInventory();
						String[] text = this.vc.getText(action.split(":")[1]);
						player.sendMessage(text);
						break;
					}
				}
				
			}
		}
		
		return true;
	}
	
	@EventHandler
    public void onInventoryClick(InventoryClickEvent event){
		if(event.getInventory().getHolder() instanceof iHelpInventory){
			event.setCancelled(true);
			if(!(event.getClickedInventory().getHolder() instanceof iHelpInventory))return;
			
			Player player = (Player) event.getWhoClicked();
			
			int clickPosition = event.getSlot();
			String invName = ((iHelpInventory)event.getClickedInventory().getHolder()).getName();
			
			String target = this.vc.getTarget(invName, clickPosition);
			if(target==null)return;
			
			switch(target.split(":")[0]){
			case "view":
				Inventory nextView = this.vc.getViewWithName(target.split(":")[1]);
				player.openInventory(nextView);
				break;
			case "text":
				player.closeInventory();
				String[] text = this.vc.getText(target.split(":")[1]);
				player.sendMessage(text);
				break;
			case "command":
				player.closeInventory();
				Map<String, Object> command = this.vc.getCommand(target.split(":")[1]);
				String cmd = (String)command.get("command");
				
				cmd = cmd.replace("{%player%}", player.getName());
				
				boolean csl = (boolean)command.get("console");
				if(csl){
					this.getServer().dispatchCommand(this.getServer().getConsoleSender(), cmd);
				}else{
					this.getServer().dispatchCommand(player, cmd);
				}
				break;
			case "exit":
				player.closeInventory();
			default:
				player.closeInventory();
			}
		}
	}
	
}
